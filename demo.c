#include "timer.h"
#include "cTask.h"

/* �жϺ��� */
void TIM3_IRQHandler(void)
{
    if (TIM_GetITStatus(TIM3, TIM_IT_Update) != RESET)
    {
        TIM_ClearITPendingBit(TIM3, TIM_IT_Update);

        cTASK_Remarks();
    }
}

int main(void)
{
    OLED_Init();
    TIME_Init();
    KEY_Init();
	
	cTASK_Init();

    cTASK_Creat(KEY_ScanTask, 10, 5);
    cTASK_Creat(OLED_ShowTask, 100, 10);
	
    cTASK_Start();
}
